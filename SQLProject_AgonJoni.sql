
-- SQL & DataWareHouse Module 
-- Last HomeWork: SQL Database for Salary management for employees across the globe.


DROP DATABASE IF EXISTS SQLProject_AgonJoni
GO
CREATE DATABASE SQLProject_AgonJoni
GO 

USE SQLProject_AgonJoni
GO

DROP TABLE IF EXISTS [Location]
CREATE TABLE [Location]
(
	ID INT IDENTITY(1,1) NOT NULL,  
	CountryName nvarchar(100) NULL, 
	Continent nvarchar(100) NULL, 
	Region NVARCHAR(100) NULL,
	CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([ID] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Seniority Level Table. 
DROP TABLE IF EXISTS [SeniorityLevel]
CREATE TABLE [SeniorityLevel]
(
	ID INT IDENTITY(1,1) NOT NULL, 
	[Name] NVARCHAR(100) NOT NULL, 
	CONSTRAINT [PK_SeniorityLevel] PRIMARY KEY ([ID] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Department Table. 
DROP TABLE IF EXISTS [Department]
CREATE TABLE [Department]
(
	ID INT IDENTITY(1,1) NOT NULL,  
	[Name] NVARCHAR(100) NOT NULL
	CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED ([ID] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-- Employee Table.
DROP TABLE IF EXISTS [Employee]
CREATE TABLE [Employee]
(
	ID INT IDENTITY(1, 1) NOT NULL,  
	FirstName nvarchar(100) NOT NULL, 
	LastName nvarchar(100) NOT NULL, 
	LocationID INT NOT NULL,
	SenioryLevelID INT NOT NULL, 
	DepartmentID INT NOT NULL
	CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([ID] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

DROP TABLE IF EXISTS [Salary]
CREATE TABLE [Salary]
(
	ID INT IDENTITY(1,1) NOT NULL,  
	EmployeeID nvarchar(100) NOT NULL, 
	[Month] smallint NOT NULL, 
	[Year] smallint NOT NULL, 
	GrossAmount decimal(18,2) NOT NULL, 
	NetAmount decimal(18,2) NOT NULL, 
	RegularWorkAmount decimal(18,2) NOT NULL, 
	BonusAmount decimal(18,2) NOT NULL, 
	OvertimeAmount decimal(18,2) NOT NULL, 
	VacationDays smallint NOT NULL, 
	SickLeaveDays smallint NOT NULL,	
	CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED ([ID] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Seniority levels should be inserted manually into the Table Seniority Level:

INSERT INTO dbo.SeniorityLevel ([Name])
	values('Junior'),
		('Intermedia'),
		('Senior'),
		('Lead'),
		('Project Manager'),
		('Division Manager'),
		('Office Manager'),
		('CEO'),
		('CTO'),
		('CIOs')
GO

-- 2.
-- LIST OF LOCATION SHOULD BE IMPORTED from Application.Countries table in WideWordImporters database.
-- Table should contain 190 recirds after import into the Table Location. 

INSERT INTO [Location]
SELECT 
	TOP 190 CountryName, Continent, Region 
from 
	WideWorldImporters.Application.Countries
GO


-- 3. The Name of the Departments should be inserted Manually 


INSERT INTO Department ([Name])
VALUES 
	('Personal Banking & Operations'),
	('Digital Banking Department'),
	('Retail Banking & Marketing Department'),
	('Wealth management & Third Party Products'),
	('International Banking Division & DFB'),
	('Treasury'),
	('Information Technology'),
	('Corporate Communications'),
	('Support Service & Branch Expansion'),
	('Human Resources')
GO



-- START OF THE BEST.

DROP TABLE IF EXISTS #Names
CREATE TABLE #Names
(
	ID INT IDENTITY(1,1) NOT NULL,
	FirstName nvarchar(50) NOT NULL, 
	LastName nvarchar(50) NOT NULL
)
GO 


INSERT INTO #Names([FirstName], [LastName])
SELECT

	FullName = SUBSTRING(FullName, 1, (CHARINDEX(' ', FullName + '') -1)), 
	FullName = SUBSTRING(Fullname, LEN(SUBSTRING(Fullname, 1, (CHARINDEX(' ', FullName + '') + 1))), len(FullName))

	/*
	FullName = left(fullname, charindex(' ', fullname)),
	FullName = SUBSTRING(fullname, CHARINDEX(' ', FullName) + 1, LEN(fullname) - (Charindex(' ', FullName) - 1))
	*/
FROM 
	WideWorldImporters.Application.People
GO

-- 291
-- now the table #name should be trimmed for a) Empty white spaces and b) Dropping Middle Names, and then, Capitalize on the first Letter of the FirstName and LastNames and lower the rest, 
-- plus c) divide the FirstNames 
-- and FirstName and LastNames that have both characters with (-) in the middle should remain as such and the first letteres should be capitlized as well. 
-- This all depends on the business rules, however in absence of such rules we follow the common sense of cultural belonging for each individuals (e.x. Asians may have longer names with '-' in between names, therefore they should be kept).
-- Normally middle should be dropped. the datatype for First and Last names should be choosen as NVARCHAR as there are non Latin letters of alphabet used. 

-- No empty spaces before and after First names and Last Names.

-- step a) - Trim the empty spaces at the leading and trailing places for FirstName and LastName 

DROP PROCEDURE IF EXISTS sp_TrimmingNames
GO
CREATE PROCEDURE sp_TrimmingNames
(
	@ID INT
)
AS BEGIN

	UPDATE 
		#Names
	SET 
		LastName =
		CASE 
			WHEN LastName like '% %'  -- substring(LastName, 1, 1) + SUBSTRING(lastName, len(lastName), 1)
			THEN LTRIM(RTrim(LastName))
		ELSE 
			LASTNAME
		END, 
		FirstName  = 
		CASE 
			WHEN FirstName like '% %'  -- substring(LastName, 1, 1) + SUBSTRING(lastName, len(lastName), 1)
			THEN LTRIM(RTrim(FirstName))
		ELSE 
			FirstName
		END
	WHERE 
		ID = @ID

		-- Here we drop the middlename from the LastName 

	UPDATE
	#Names
		SET
			LastName = 
			CASE 
				WHEN LastName = left(lastName, charindex(' ', LastName)) 
				THEN Replace(Left(LastName, charindex(' ', LastName)), Left(LastName, charindex(' ', LastName)), '') + (SUBSTRING(LastName, CHARINDEX(' ', LastName) + 1, LEN(LastName) - (Charindex(' ', LastName) - 1)))
			ELSE 
				LTRIM(RTRIM(Replace(Left(LastName, charindex(' ', LastName)), Left(LastName, charindex(' ', LastName)), '') + (SUBSTRING(LastName, CHARINDEX(' ', LastName) + 1, LEN(LastName) - (Charindex(' ', LastName) - 1)))))
			END
	WHERE 
		ID = @ID
END
GO

DECLARE @MaxID INT
DECLARE @Looping INT


SELECT @MaxID = MAX(ID) FROM #Names 
SET @Looping = 1


WHILE(@Looping <= @MaxID)
	BEGIN
		EXEC sp_TrimmingNames
		@ID = @Looping

	set @Looping = @Looping + 1
	END
GO


-- Meanwhile we could either drop the first part of the Name with (-) char in the middle. But such names may be custom to some parts of Asia, therefore dropping them
-- might change the identity of the individuals. Therefore, in order to take them off it is important that IT lady/gentelman talk to the people responsible about the 
-- bussiness rules concerning policies of trimming of person's names. Therefore, in the given case we keep the Asian's names with (-) in between the names, but we must make sure 
-- that the the first part of the Name is with large letter and the first letter of the second part of the (-) name is capitilized as well. 
-- This process could be arranged and solved with a Function and a Store Procedure. 

-- This function goes through the string and capilizes every letter of the Names and Last names that is a before and after the dash.
-- what is important is that in a dataset, some of the names use letters that fall outside of standard language letters, and in order to maintain the same letters for all different names, 
-- we use the NVARCHAR data type.

-- Hence, the function is created.

CREATE OR ALTER FUNCTION [dbo].[ConvertFirstLetterinCapitalInNames](@Text NVARCHAR(50)) 
RETURNS NVARCHAR(50) 
AS BEGIN
 DECLARE @Index INT;
 DECLARE @FirstChar CHAR(1);
 DECLARE @LastChar CHAR(1);
 DECLARE @String NVARCHAR(50);

   SET @String = LOWER(@Text);
   SET @Index = 1;
 
 WHILE @Index <= LEN(@Text)
       BEGIN
           SET @FirstChar = SUBSTRING(@Text, @Index, 1);
           SET @LastChar = 
				CASE WHEN @Index = 1
				   THEN ' '
                   ELSE SUBSTRING(@Text, @Index - 1, 1)
				END;
           IF @LastChar IN('-', ' ')
               BEGIN
                   IF @FirstChar != ''''
                       SET @String = STUFF(@String, @Index, 1, UPPER(@FirstChar));
			    END;
           SET @Index = @Index + 1;
       END;
    RETURN @String;
   END;
GO

-- To make the first letters of the Names and LastNames of the the first and second part of the names a store procedure is created that runs through
-- the names in the dataset and updates them accordingly in line with the above-created function.

DROP PROCEDURE IF EXISTS [dbo].[CapitaliseNames]
GO
CREATE PROCEDURE [dbo].[CapitaliseNames]
(
	@ID INT
)
AS BEGIN 
	/*
	UPDATE 
		#Names5
	SET
		FirstName = UPPER(LEFT(FirstName, 1)) + LOWER(SUBSTRING(FirstName, 2, Len(FirstName))), 
		LastName = UPPER(LEFT(LastName, 1)) + LOWER(SUBSTRING(LastName, 2, Len(LastName)))
	WHERE 
		ID = @ID

	*/
		-- the update calls the function created above to capitalize the first and last names of after the dashes.
	UPDATE 
		#Names
	SET
		FirstName = [dbo].[ConvertFirstLetterinCapitalInNames](FirstName),
		LastName = [dbo].[ConvertFirstLetterinCapitalInNames](LastName)
	WHERE 
		ID = @ID
END
GO

DECLARE @MaximumID INT
DECLARE	@LoopingNumber INT

SELECT @LoopingNumber = 1
SELECT @MaximumID = max(ID) from #Names

WHILE(@LoopingNumber <= @MaximumID)
	BEGIN 
	
		EXEC [dbo].[CapitaliseNames] 
		@ID = @LoopingNumber
	
		SET @LoopingNumber = @LoopingNumber + 1;
	
	END
GO	

INSERT INTO Employee(FirstName, LastName, LocationID, SenioryLevelID, DepartmentID)
	SELECT FirstName, LastName, 0 as LocationID, 0 as SeniorityLevelID, 0 as DepartmentID  
	FROM #Names
GO

UPDATE 
	N
SET 
	LocationID = LocationID1,
	SenioryLevelID = SenioryLevelID1, 
	DepartmentID = N.DepartmentID1		
from 
	(
	select 
		E.LocationID, E.SenioryLevelID, E.DepartmentID,
		NTILE(190) OVER(ORDER BY E.ID) AS LocationID1, 
		NTILE(10) OVER(ORDER BY E.ID) AS SenioryLevelID1, 
		NTILE(10) OVER(ORDER BY E.ID) AS DepartmentID1	
	FROM Employee AS E
	) AS N
GO


DROP PROCEDURE IF EXISTS CreatingAndInsertingSalaries
GO
CREATE PROCEDURE CreatingAndInsertingSalaries
AS 
BEGIN
	DROP TABLE IF EXISTS #Salary
	CREATE TABLE #Salary
	(
		ID INT IDENTITY(1,1) NOT NULL,  
		EmployeeID INT NULL, 
		[Month] SMALLINT NULL, 
		[Year] SMALLINT NULL, 
		GrossAmount DECIMAL(18,2) NULL, 
		NetAmount DECIMAL(18,2) NULL, 
		RegularWorkAmount DECIMAL(18,2) NULL, 
		BonusAmount DECIMAL(18,2) NULL, 
		OvertimeAmount DECIMAL(18,2) NULL, 
		VacationDays SMALLINT NULL, 
		SickLeaveDays SMALLINT null
	)	

	DROP TABLE IF EXISTS #Years
	CREATE TABLE #Years
	(
		[Years] SMALLINT
	)
	
	DROP TABLE IF EXISTS #Months
	CREATE TABLE #Months
	(
		[Months] SMALLINT
	)
	
	
	DECLARE @Years SMALLINT = 2001
	DECLARE @EndYear SMALLINT = 2020

	

	-- Filling the 20 years of working.
	WHILE(@Years <= @EndYear) 
		BEGIN 
			INSERT INTO #Years([Years])	
				VALUES(@Years)
			SET @Years = @Years + 1
		END

	
	-- Filling the months.
	DECLARE @Months SMALLINT = 1
	DECLARE @EndMonth SMALLINT = 12
	
	WHILE(@Months <= @EndMonth)
		BEGIN
			INSERT INTO #Months([Months])
				VALUES(@Months)
			SET @Months = @Months + 1
		END

	-- The scalar values assign only one static value, and surprisingly behaves awkwardly. Therefore, it is better to use
	-- the random function as a whole right in the code: 30001 + ABS(CHECKSUM(NewID())) % 30000. 
	-- FILLING THE MONTHS
	-- DECLARE @RandomGrossAmount DECIMAL(18,2)
	-- SELECT @RandomGrossAmount = 30001 + ABS(CHECKSUM(NewID())) % 30001 

	-- The combination of total rows should be at 1111 employees * 12 months * 20 years = 266,640 rows. 

	INSERT INTO #Salary(EmployeeID, [Month], [Year], [GrossAmount])
		SELECT 
			E.ID, M.Months, Y.Years, 30001 + ABS(CHECKSUM(NewID())) % 30001
		FROM 
			Employee AS E
			CROSS JOIN #Months AS M 
			CROSS JOIN #Years AS Y
		ORDER BY 
			e.ID asc, m.Months desc, y.Years desc 

	-- select max([GrossAmount]) from #Salary -- 60,000
	-- select min([GrossAmount]) from #Salary -- 30,001
	
	-- Inserting NetAmount worth 90% of GrossAmount
		UPDATE
			#Salary
		SET 
			[NetAmount] = [GrossAmount] * 0.90
		

	-- Inserting RegularWorkAmount worth 80% for all employees and months. 
		UPDATE
			#Salary
		SET 
			[RegularWorkAmount] = [NetAmount] * 0.80
		
	-- Inserting BonusAmount the difference between NetAmount and RegularWorkAmount for Odd months
		UPDATE 
			#Salary
		SET 
			[BonusAmount] =
				CASE 
					WHEN [Month] % 2 <> 0 
					THEN [NetAmount] - [RegularWorkAmount] 
				ELSE
					0
				END
	
	-- Inserting OverTimeAmount Should be the difference between the NetAmount and RegularWorkAmount 

		UPDATE 
			#Salary
		SET 
			[OvertimeAmount] =
				CASE 
					WHEN [Month] % 2 = 0 
					THEN [NetAmount] - [RegularWorkAmount] 
				ELSE
					0
				END

	
	-- Additionally random vacation days and sickLeaveDays should be generated with the following scrpit:
		UPDATE 
			#Salary
		SET 
			VacationDays = 
				CASE 
					WHEN (EmployeeID + [Month] + [Year]) % 5 = 1
					THEN VacationDays + (EmployeeID % 2)
				ELSE 
					0
				END

	-- And for SickLeaves 
		UPDATE 
			#Salary
		SET 
			SickLeaveDays = 
				CASE 
					WHEN (EmployeeID + [Month] + [Year])%5 = 2
					THEN EmployeeID%8
				ELSE 
					0
				END, 
			VacationDays =
				CASE 
					WHEN (EmployeeID + [Month] + [Year])%5 = 2
					THEN [VacationDays] + (EmployeeID%3)
				ELSE 
					0
				END


	-- All Employees use 10 vacation days in July and 10 Vacation days in December 
		UPDATE 
			#Salary
		SET 
			[VacationDays] = 10
		WHERE 
			[Month] = 7 OR [Month] = 12


	INSERT INTO Salary([EmployeeId], [Month], [Year], [GrossAmount], [NetAmount], [RegularWorkAmount], [BonusAmount], [OvertimeAmount], [VacationDays], [SickLeaveDays])
		SELECT 
			S.EmployeeID, S.[Month], S.[Year], S.[GrossAmount], S.[NetAmount], S.[RegularWorkAmount], S.[BonusAmount], S.[OvertimeAmount], S.[VacationDays], S.[SickLeaveDays]
		FROM 
			#Salary AS S

END
GO

EXEC CreatingAndInsertingSalaries

-- if it functions correctly then the following code should return 0:
	SELECT 
		* 
	FROM 
		Salary
	WHERE 
		[NetAmount] <> ([RegularWorkAmount] + [BonusAmount] + [OvertimeAmount])
	GO 


SELECT * FROM Department
GO
SELECT * FROM Employee
GO
SELECT * FROM [Location]
GO
SELECT * FROM SeniorityLevel
GO 
SELECT * FROM Salary
GO

